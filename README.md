# NextJS App

This app is purely designed to work as a static web app and without any server.
NextJS provide `next export` command, which after doing `npm run build` generates a list of staitc files in `out` directory.
https://nextjs.org/docs/advanced-features/static-html-export prvoides clear docs and for using next export, you have to give up certain nextjs features.
The Idea is host all the contents on the `out` directory in a Github/Bitbucket as a static repository and access it without any servers.
Need to build a bitbucket CI/CD pipeline script in `bitbucket-pipelines.yml`, that will automatically run `next build && next export` and then upload the contents of the `out` directory to Bitbucket static repo.
