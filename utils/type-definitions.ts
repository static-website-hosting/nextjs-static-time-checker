export type timeCheckerInput = {
    numberOfDays: number
    locationCode: string
}
export type timeCheckerOutput = {
    raguYamaData?: RaguYamaDataType[],
    ashtamiData?: AshtamiDataType[]
}

export type RaguYamaDataType = {
    date: string
    locationCode: string
    ra: {
        end: string
        start: string
    }
    ya: {
        end: string
        start: string
    }
}

export type AshtamiDataType = {
    endDate: string
    endTime: string
    startDate: string
    startTime: string
}