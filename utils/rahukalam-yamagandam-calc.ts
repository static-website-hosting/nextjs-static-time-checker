const SunCalc = require('suncalc');
const moment = require('moment');
const momentTimeZone = require('moment-timezone');
const locationMasterData: LocationDataType[] = require('./location-data.json');
import { RaguYamaDataType, timeCheckerInput } from "./type-definitions"

type raguYamaInput = {
  date: any,
  sunRiseTimeInEPOCH: number
  sunSetTimeInEPOCH: number
  locationTimeZone: string
}

type LocationDataType = {
  "locationCode": string
  "latitude": number
  "longitude": number
  "locationTimeZone": string

}

export const getConsolidatedData = (incrementor:number, locationCode: string) => {
  const { locationCode: dataLocation,
    locationTimeZone,
    latitude, longitude} = getLocationSpecificInfo(locationCode);
  //const timeZoneDateTime = momentTimeZone().tz(locationTimeZone);
  let date: any = incrementedDate(incrementor, locationTimeZone);
  //const sunData = getSunDataFromSunCalc(timeZoneDateTime, latitude, longitude);
  const sunData = getSunDataFromSunCalc(date, latitude, longitude);
  const sunRiseTimeInEPOCH = moment(sunData.sunrise).unix();
  const sunSetTimeInEPOCH = moment(sunData.sunset).unix();
  
  var yamagandam = calculateYamagandamForDay({date, sunRiseTimeInEPOCH,sunSetTimeInEPOCH, locationTimeZone});
  var rahuKalam = calculateRahuKalamForDay({date, sunRiseTimeInEPOCH,sunSetTimeInEPOCH, locationTimeZone});
  const rahuYamaTimes = {
    ra: rahuKalam,
    ya: yamagandam,
    date: date.format("MMM DD ddd"),
    locationCode: dataLocation
  };
  return rahuYamaTimes;
};


export const getDataForMultipleDays = ({numberOfDays = 10, locationCode}:timeCheckerInput): RaguYamaDataType[] => {
    let incrementor = 0;
    let dataForMultipleDays = [];
    while(incrementor <= numberOfDays) {
      dataForMultipleDays.push(getConsolidatedData(incrementor, locationCode));
      incrementor++;
    }
  //console.log(dataForMultipleDays);
  return dataForMultipleDays;
}

const incrementedDate = (incrementor = 0, preferredTimeZone:string) => {
  let date = momentTimeZone().tz(preferredTimeZone).add(incrementor, 'days');
  return date;
}



const calculateRahuKalamForDay = ({date, sunRiseTimeInEPOCH,sunSetTimeInEPOCH, locationTimeZone}: raguYamaInput) => {
  var dayOfWeek = date.day();
  const lengthOfDay = sunSetTimeInEPOCH - sunRiseTimeInEPOCH;
  //from Wiki https://en.wikipedia.org/wiki/Rahukaalam
  var singleMhuruthTimeForTheDay = lengthOfDay / 8;
  var mhuruthUnit = getRahuKalamMhurtamForGivenDay(dayOfWeek);
  var rahuEndTimeMhu = (singleMhuruthTimeForTheDay*mhuruthUnit) + sunRiseTimeInEPOCH;
  var rahuStartTimeMhu = (singleMhuruthTimeForTheDay*(mhuruthUnit-1)) + sunRiseTimeInEPOCH;
  var rahuEndTimeLocalTime = getLocalTimeFromEpoch(rahuEndTimeMhu, locationTimeZone);
  var rahuStartTimeLocalTime = getLocalTimeFromEpoch(rahuStartTimeMhu, locationTimeZone);
  //return `${rahuStartTimeLocalTime} to ${rahuEndTimeLocalTime}`;
  return {
    start: rahuStartTimeLocalTime,
    end: rahuEndTimeLocalTime
  }
}
type d = {

}
const calculateYamagandamForDay = ({date, sunRiseTimeInEPOCH,sunSetTimeInEPOCH, locationTimeZone}:raguYamaInput) => {
  var dayOfWeek = date.day();
  const lengthOfDay = sunSetTimeInEPOCH - sunRiseTimeInEPOCH;
  var singleMhuruthTimeForTheDay = lengthOfDay / 8;
  var yemaGandamMhuruthUnit = getYemagandamMhurtamForGivenDay(dayOfWeek);
  var yemaGandamEndTimeMhu = (singleMhuruthTimeForTheDay*yemaGandamMhuruthUnit) + sunRiseTimeInEPOCH;
  var yemaGandamStartTimeMhu = (singleMhuruthTimeForTheDay*(yemaGandamMhuruthUnit-1)) + sunRiseTimeInEPOCH;
  var yemaGandamEndTimeLocalTime = getLocalTimeFromEpoch(yemaGandamEndTimeMhu, locationTimeZone);
  var yemaGandamStartTimeLocalTime = getLocalTimeFromEpoch(yemaGandamStartTimeMhu, locationTimeZone);

  //return `${yemaGandamStartTimeLocalTime} to ${yemaGandamEndTimeLocalTime}`;
  return {
    start:  yemaGandamStartTimeLocalTime ,
    end: yemaGandamEndTimeLocalTime
  }
}

const getLocalTimeFromEpoch = (utcSeconds: number, preferredTimeZone: string) => {
  var date = new Date(0); // The 0 there is the key, which sets the date to the epoch
  date.setUTCSeconds(utcSeconds);
  var formatString = 'HH:mm:ss';
  return momentTimeZone(date,formatString).tz(preferredTimeZone).format(formatString);
}

const getSunDataFromSunCalc = (timeZoneDateTime: any, latitude: number, longitude: number) =>{
  const sunData = SunCalc.getTimes(timeZoneDateTime, latitude, longitude);
  return sunData;
}

const getRahuKalamMhurtamForGivenDay = (dayOfWeek:number) => {

//Sunday - 8th Muhurt (Unit)
//Monday - 2nd Muhurt
//Tuesday - 7th Muhurt
//Wednesday - 5th Muhurt
//Thursday - 6th Muhurt
//Friday - 4th Muhurt
//Saturday - 3rd Muhurt
  var weekdayRahuKalamMhurthMapping = new Array(7);
  weekdayRahuKalamMhurthMapping[0] =  8;
  weekdayRahuKalamMhurthMapping[1] = 2;
  weekdayRahuKalamMhurthMapping[2] = 7;
  weekdayRahuKalamMhurthMapping[3] = 5;
  weekdayRahuKalamMhurthMapping[4] = 6;
  weekdayRahuKalamMhurthMapping[5] = 4;
  weekdayRahuKalamMhurthMapping[6] = 3;

  return weekdayRahuKalamMhurthMapping[dayOfWeek];

}

const getYemagandamMhurtamForGivenDay = (dayOfWeek:number) => {

//Sunday - 8th Muhurt (Unit)
//Monday - 2nd Muhurt
//Tuesday - 7th Muhurt
//Wednesday - 5th Muhurt
//Thursday - 6th Muhurt
//Friday - 4th Muhurt
//Saturday - 3rd Muhurt
  var weekdayYemagandamMhurthMapping = new Array(7);
  weekdayYemagandamMhurthMapping[0] =  5;
  weekdayYemagandamMhurthMapping[1] = 4;
  weekdayYemagandamMhurthMapping[2] = 3;
  weekdayYemagandamMhurthMapping[3] = 2;
  weekdayYemagandamMhurthMapping[4] = 1;
  weekdayYemagandamMhurthMapping[5] = 7;
  weekdayYemagandamMhurthMapping[6] = 6;

  return weekdayYemagandamMhurthMapping[dayOfWeek];

}

const getLocationSpecificInfo = (locationCode: string) => {
  let locationData = {
    "locationCode": "sfo",
		"latitude": 37.7,
		"longitude": -122.42,
		"locationTimeZone": "America/Los_Angeles"
  };

  const filtered:LocationDataType[] = locationMasterData.filter(locationDatum => locationDatum.locationCode === locationCode);
  if(Array.isArray(filtered) && filtered.length  > 0) {
    locationData = filtered[0];
  }
  return locationData;
}

//console.log(getConsolidatedData(0, "SFO"));
//console.log(getDataForMultipleDays(10, "SFO"))
//exports.getConsolidatedData = getConsolidatedData;
