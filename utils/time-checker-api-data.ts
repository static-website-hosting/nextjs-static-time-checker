import { getDataForMultipleDays } from "./rahukalam-yamagandam-calc";
import { getUpcomingAshtmiDates } from "./asthami-calculator";
import { timeCheckerInput, timeCheckerOutput } from "./type-definitions"


export function getRaguYamaAshtamiData({numberOfDays = 1, locationCode= "sfo"}: timeCheckerInput) : timeCheckerOutput {

  let raguYamaData = getDataForMultipleDays({numberOfDays, locationCode});
  let ashtamiData = getUpcomingAshtmiDates();
  let outputData = {
    raguYamaData,
    ashtamiData,
  };
  return outputData;
}
