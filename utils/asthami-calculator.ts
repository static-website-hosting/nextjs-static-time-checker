import momentTimeZone from "moment-timezone";
import ashtamiData from "./ashtami-data.json";
import { AshtamiDataType } from "./type-definitions";

export const getUpcomingAshtmiDates = (): AshtamiDataType[] | undefined => {
  const currentDate = new Date();
  let currentMonth = currentDate.getMonth() + 1;
  if (currentMonth == 12) {
    currentMonth = 1;
  }

  const currentMonthData = ashtamiData.find(
    (data) => data.month === currentMonth
  );
  let nextMonth = currentMonth + 1;
  if (currentMonth === 12) {
    nextMonth = 1;
  }
  const nextMonthData = ashtamiData.find((data) => data.month === nextMonth);
  if (currentMonthData && nextMonthData) {
    //combining two months of data
    const upcomingAshtamiDates = [
      ...currentMonthData.dates,
      ...nextMonthData.dates,
    ];
    //console.log(upcomingAshtamiDates)
    const outputData: AshtamiDataType[] = [];
    upcomingAshtamiDates.forEach((date) => {
      if (new Date(date.start).getTime() < new Date().getTime()) {
        console.log("Skipping since these are past dates");
      } else {
        let startDateObj = momentTimeZone(Date.parse(date.start)).tz(
          "America/Los_Angeles"
        );
        let endDateObj = momentTimeZone(Date.parse(date.end)).tz(
          "America/Los_Angeles"
        );
        let data = {
          startDate: startDateObj.format("MMM DD ddd"),
          startTime: startDateObj.format("HH:mm:ss"),
          endDate: endDateObj.format("MMM DD ddd"),
          endTime: endDateObj.format("HH:mm:ss"),
        };
        //outputData.push((date.start)+" to "+(date.end));
        outputData.push(data);
      }
    });
    return outputData;
  }
};

//console.log(momentTimeZone(Date.parse(data.start)).format("MMM DD HH:mm:ss"))
//console.log(getUpcomingAshtmiDates());
