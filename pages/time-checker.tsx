import React, { useState, useEffect } from "react";
import { getRaguYamaAshtamiData } from "../utils/time-checker-api-data";
import {
  timeCheckerInput,
  RaguYamaDataType,
  AshtamiDataType,
} from "../utils/type-definitions";
import { useRouter } from "next/router";

const TimeChecker = () => {
  const [isLoading, setLoading] = useState(true);
  const [raguYamaData, setRaguYamaData] = useState<RaguYamaDataType[]>([]);
  const [ashtamiData, setAshtamiData] = useState<AshtamiDataType[]>([]);
  const [locationCode, setLocationCode] = useState<string>("sfo");
  const [numberOfDays, setNumberOfDays] = useState<number>(5);
  const router = useRouter();

  useEffect(() => {
    try {
      console.log(
        "NEXT JS ROUTER QUERY ============>" + JSON.stringify(router.query)
      );
      const {
        locationCode: locationCodeFromQuery,
        numberOfDays: numberOfDaysFromQuery,
      } = router.query;
      setLocationCode(locationCodeFromQuery as string);
      setNumberOfDays(parseInt(numberOfDaysFromQuery as string));
      const inputForTimeChecker: timeCheckerInput = {
        locationCode: locationCodeFromQuery as string,
        numberOfDays: parseInt(numberOfDaysFromQuery as string),
      };
      let { raguYamaData, ashtamiData } = getRaguYamaAshtamiData(inputForTimeChecker);
      setLoading(false);
      if(raguYamaData) {
        setRaguYamaData(raguYamaData);
      }
      if(ashtamiData) {
        setAshtamiData(ashtamiData);
      }
    } catch (error) {
      console.log(error);
    }
  }, [router.query]);

  if (isLoading) {
    return <div className="container">Still loading!</div>;
  } else {
    return (
      <div className="container">
        Location :: {locationCode}
        <table className="table-striped">
          <tbody>
            <tr>
              <th>Day</th>
              <th>Type</th>
              <th>Start Time</th>
              <th>End Time</th>
            </tr>
            {_renderList({raguYamaData})}
          </tbody>
        </table>
        <h3>Ashtami Details</h3>
        <table className="table-striped">
          <tbody>
            <tr>
              <th>Start Date</th>
              <th>Start Time</th>
              <th>End Date</th>
              <th>End Time</th>
            </tr>
            {_renderAshtamiData({ashtamiData})}
          </tbody>
        </table>
      </div>
    );
  }
};

const _renderList = ({raguYamaData = []}: {raguYamaData: RaguYamaDataType[]} ) => {
  return (
    raguYamaData.length > 0 &&
    raguYamaData.map((datum, index) => {
      return (
        // eslint-disable-next-line react/jsx-key
        <React.Fragment>
          <tr>
            <td rowSpan={2}>{datum.date}</td>
            <td>ra</td>
            <td>{datum.ra.start}</td>
            <td>{datum.ra.end}</td>
          </tr>
          <tr>
            <td>Ya</td>
            <td>{datum.ya.start}</td>
            <td>{datum.ya.end}</td>
          </tr>
        </React.Fragment>
      );
    })
  );
};
const _renderAshtamiData = ({ashtamiData = []}:{ashtamiData: AshtamiDataType[]}) => {
  return (
    ashtamiData.length > 0 &&
    ashtamiData.map((datum, index) => {
      return (
        // eslint-disable-next-line react/jsx-key
        <tr>
          <td>{datum.startDate}</td>
          <td>{datum.startTime}</td>
          <td>{datum.endDate}</td>
          <td>{datum.endTime}</td>
        </tr>
      );
    })
  );
};

export default TimeChecker;
